/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angela.apidummy.repository;

import com.angela.apidummy.model.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 *
 * @author ANGELA
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	
	List<User> findByUsername(String username);
	List<User> findByEmail(String email);
	
	User findByUsernameOrEmail(String username, String email);
}
