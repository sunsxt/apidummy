/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angela.apidummy.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.angela.apidummy.dto.PostCreateDto;
import com.angela.apidummy.dto.PostGetDto;
import com.angela.apidummy.dto.ReturnDto;
import com.angela.apidummy.dto.ReturnPostDto;
import com.angela.apidummy.service.PostService;

/**
 *
 * @author ANGELA
 */
@RestController
@RequestMapping("/posts")
public class PostController {
    
    @Autowired
    PostService postService;
    
    @ResponseBody
    @CrossOrigin(origins = "*", methods= {RequestMethod.GET})
    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<ReturnPostDto> listAll() {
    	ReturnPostDto returnPostDto = new ReturnPostDto();
    	try {
    		List<PostGetDto> postDto = postService.findAll();
    		returnPostDto.setCode(String.valueOf(HttpStatus.OK.value()));
    		returnPostDto.setPostCreateDto(postDto);
			return new ResponseEntity<>(returnPostDto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			returnPostDto.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			return new ResponseEntity<>(returnPostDto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	
    }
    
    @ResponseBody
    @CrossOrigin(origins = "*", methods = { RequestMethod.POST })
    @PostMapping(path = "/create", consumes = "application/json")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<ReturnDto> create(@RequestBody PostCreateDto postCreateDto){
    	ReturnDto returnDto = new ReturnDto();
    	try {
			postService.create(postCreateDto);
			returnDto.setCode(String.valueOf(HttpStatus.OK.value()));
			returnDto.setResult("Correct create post");
			return new ResponseEntity<>(returnDto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			returnDto.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			returnDto.setResult("Error: " + e.getMessage());
			return new ResponseEntity<>(returnDto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    
    @ResponseBody
    @DeleteMapping(path = "/delete")
    public String delete(@RequestParam("postId") Integer postId){
    	
    	try {
    		postService.delete(postId);
			
		} catch (Exception e) {
			return "Error: "+e.getMessage();
		}
		
    	return "Se eliminó correctamente";
    }
    
}
