/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angela.apidummy.service;

import java.sql.SQLException;
import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.angela.apidummy.configuration.EmailUtil;
import com.angela.apidummy.dto.LoginDto;
import com.angela.apidummy.dto.ReturnDto;
import com.angela.apidummy.dto.UserCreateDto;
import com.angela.apidummy.model.User;
import com.angela.apidummy.repository.UserRepository;
import com.angela.apidummy.utils.Constants;

/**
 *
 * @author ANGELA
 */
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    
    @Autowired
    private Environment env;
    
    public User findById(Integer id) {
    	return userRepository.findById(id).get();
    }
    
    public void create (UserCreateDto userDto) throws Exception {
    	
    	if(userDto == null) {
    		throw new Exception("UserDto is null");
    	}
    	
    	if(userDto.getUsername()== null) {
    		throw new Exception("Username is null");
    	}
    	
    	if(userDto.getEmail()== null) {
    		throw new Exception("Email is null");
    	}
    	
    	if(!userRepository.findByUsername(userDto.getUsername()).isEmpty()) {
    		throw new Exception("Username already exist");
    	}
    	
    	if(!userRepository.findByEmail(userDto.getEmail()).isEmpty()) {
    		throw new Exception("Email already exist");
    	}
    	
    	if(userDto.getPassword()== null) {
    		throw new Exception("Password is null");
    	}
    	
    	if(userDto.getFullname()== null) {
    		throw new Exception("Fullname is null");
    	}
    	
    	User user = dtoToEntity(userDto);
    	
    	try {
    		userRepository.save(user);
    	}catch (Exception e) {
			throw new Exception(e);
		}
    	
    }
    
    private User dtoToEntity(UserCreateDto userDto) {
    	User user = new User();
    	user.setUsername(userDto.getUsername());
    	user.setFullname(userDto.getFullname());
    	user.setEmail(userDto.getEmail());
    	user.setImage(userDto.getImage() != null ? userDto.getImage() : null);
    	user.setPassword(encodePassword(userDto.getPassword()));
    	user.setPhone(userDto.getPhone() != null ? userDto.getPhone() : null);
    	user.setPosition(userDto.getPosition() != null ? userDto.getPosition() : null);
    	return user;
    }
    
    private String encodePassword(String password) {
    	return BCrypt.hashpw(password, BCrypt.gensalt(12));
    }
    
    public boolean validatePassword (String passwordDto, String passwordBD) {
    	return BCrypt.checkpw(passwordDto, passwordBD);
    }
    
     
    public ResponseEntity<ReturnDto> login(LoginDto loginDto) throws Exception {

    	ReturnDto returnDto = new ReturnDto();
    	
    	if(loginDto == null) {
    		throw new Exception("Entity LogintDto is null");
    	}
    	
    	if(loginDto.getEmail() == null && loginDto.getUsername() == null) {
    		throw new Exception("Email and Username is null");
    	}
    	
    	if(loginDto.getPassword() == null) {
    		throw new Exception("Password is null");
    	}
    	
    	User user = userRepository.findByUsernameOrEmail(loginDto.getUsername(), loginDto.getEmail());
    	
    	if(user == null) {
    		returnDto.setCode(String.valueOf(HttpStatus.UNAUTHORIZED.value()));
			returnDto.setResult("Incorrect username and password");
			return new ResponseEntity<>(returnDto, HttpStatus.UNAUTHORIZED);
    	}
    	
    	
    	if (validatePassword(loginDto.getPassword(), user.getPassword())) {
			returnDto.setCode(String.valueOf(HttpStatus.OK.value()));
			returnDto.setResult(String.valueOf(user.getId()));
			return new ResponseEntity<>(returnDto, HttpStatus.OK);
		} else {
			returnDto.setCode(String.valueOf(HttpStatus.UNAUTHORIZED));
			returnDto.setResult("Incorrect username and password");
			return new ResponseEntity<>(returnDto, HttpStatus.UNAUTHORIZED);
		}
    
    }
    
    public void sendEmailForRestorePassword(LoginDto loginDto) throws Exception {
    	if(loginDto == null) {
    		throw new Exception("Entity LogintDto is null");
    	}
    	
    	if(loginDto.getEmail() == null && loginDto.getUsername() == null) {
    		throw new Exception("Email and Username is null");
    	}
    	
    	User user = userRepository.findByUsernameOrEmail(loginDto.getUsername(), loginDto.getEmail());
    	
    	if(user == null) {
    		throw new Exception("Username or email not exist");
    	}
    	
    	sendEmail(user);

    }
    
    private void sendEmail(User user) throws SQLException {
	    System.out.println("SimpleEmail Start");
		
	    Properties props = System.getProperties();

	    props.put("mail.smtp.host", env.getProperty("mail.smtp.host"));
	    props.put("mail.smtp.port", env.getProperty("mail.smtp.port"));
	    props.put("mail.smtp.username", env.getProperty("mail.smtp.username"));
	    props.put("mail.smtp.password", env.getProperty("mail.smtp.password"));
	    
	    props.put("mail.smtp.auth", env.getProperty("mail.smtp.auth"));
	    
	    props.put("mail.smtp.starttls.enable", env.getProperty("mail.smtp.starttls.enable"));

	    Session session = Session.getInstance(props, new javax.mail.Authenticator() {
	        protected PasswordAuthentication getPasswordAuthentication() {
	            return new PasswordAuthentication(env.getProperty("mail.smtp.username"), env.getProperty("mail.smtp.password"));
	        }
	    });
	    
	    System.out.println(user.getEmail());
	    Integer randomValue = getRadomValue();
	    updateEmailCode(user, randomValue);
	    EmailUtil.sendEmail(session, user.getEmail(), Constants.AFFAIR_EMAIL_RESTORE_PASSWORD, String.format(Constants.MSG_EMAIL_RESTORE_PASSWORD, randomValue));
    }
    
    private int getRadomValue() {
    	return (int) (Math.random() * (9999 + 1 - 1000)) + 1000;
    }
     
    private void updateEmailCode(User user, Integer code) throws SQLException {
    	user.setEmailCode(code);
		userRepository.save(user);
    }
    
    public void updatePassword(LoginDto loginDto) throws Exception {
    	if(loginDto == null) {
    		throw new Exception("Entity LogintDto is null");
    	}
    	
    	if(loginDto.getEmail() == null && loginDto.getUsername() == null) {
    		throw new Exception("Email and Username is null");
    	}
    	
    	if(loginDto.getPassword() == null) {
    		throw new Exception("Password is null");
    	}
    	User user = userRepository.findByUsernameOrEmail(loginDto.getUsername(), loginDto.getEmail());
    	user.setPassword(encodePassword(loginDto.getPassword()));
    	userRepository.save(user);
    }
    
    public boolean validateEmailCode(String username, String email, Integer code) throws Exception {
    	boolean flag = false;
    	Integer codeBd = getEmailCode(username, email);
    	
    	if(codeBd.equals(code)) {
    		flag = true;
    	}
    	
    	return flag;
    }
    
    public Integer getEmailCode(String username, String email) throws Exception {
    	
    	if(email == null && username == null) {
    		throw new Exception("Email and Username is null");
    	}
    	
    	User user = userRepository.findByUsernameOrEmail(username, email);
    	
    	if(user == null) {
    		throw new Exception("Username or email not exist");
    	}
    	
    	return user.getEmailCode();
    }

}
