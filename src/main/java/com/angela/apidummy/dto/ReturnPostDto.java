package com.angela.apidummy.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReturnPostDto {

	private String code;
	
	@JsonProperty("data")
	private List<PostGetDto> postCreateDto;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<PostGetDto> getPostCreateDto() {
		return postCreateDto;
	}

	public void setPostCreateDto(List<PostGetDto> postCreateDto) {
		this.postCreateDto = postCreateDto;
	}

	@Override
	public String toString() {
		return "ReturnPostDto [code=" + code + ", postCreateDto=" + postCreateDto + "]";
	}

}
