/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angela.apidummy.service;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.angela.apidummy.dto.PostCreateDto;
import com.angela.apidummy.dto.PostGetDto;
import com.angela.apidummy.model.Post;
import com.angela.apidummy.model.User;
import com.angela.apidummy.repository.PostRepository;

/**
 *
 * @author ANGELA
 */
@Service
public class PostService {

    @Autowired
    PostRepository postRepository;
    
    @Autowired
    UserService userService;

    public List<PostGetDto> findAll() {
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    	List<PostGetDto> postsDto = new ArrayList<PostGetDto>();
    	List<Post> posts = postRepository.findAllByOrderByIdDesc();
    	
    	if(!posts.isEmpty()) {
    		for (Post post : posts) {
    			
    			PostGetDto postDto = new PostGetDto();
    			postDto.setId(post.getId());
    			User user = userService.findById(post.getUserId().getId());
    			postDto.setName(user.getFullname() != null ? user.getFullname() : "");
    			postDto.setImage(user.getImage() != null ? user.getImage() : "");
    			postDto.setPosition(user.getPosition() != null ? user.getPosition() : "");
    			postDto.setDate(post.getDate() != null ? sdf.format(post.getDate()) : "");
    			postDto.setSummary(post.getSummary());
    			postsDto.add(postDto);
    		}
    	}
    	
    	return postsDto;
    }
    
    public void create(PostCreateDto postCreateDto) throws Exception {
    	
    	User user = new User();
    	
    	if(postCreateDto == null) {
    		throw new Exception("El post está vacío");
    	}
    	
    	try {
    		user = userService.findById(postCreateDto.getUserId());
		} catch (Exception e) {
			throw new Exception("No existe el userId insertado");
		}
    	
    	if(user == null) {
    		throw new Exception("No existe el userId insertado");
    	}
    	
    	Post post = new Post();
    	post.setUserId(user);
    	post.setDate(getDateColombiaNow());
    	post.setSummary(postCreateDto.getSummary());
    	postRepository.save(post);
    	
    }
    
    public void delete(Integer postId) {
    	
    	Optional<Post> postOp = postRepository.findById(postId);
    	
    	if(postOp != null) {
    		Post post = postOp.get();
    		postRepository.delete(post);
    	}
    	
    }

    private Date getDateColombiaNow() throws Exception {
    	Instant instant = Instant.now();
		ZoneId zoneId = ZoneId.of( "America/Bogota" );
		ZonedDateTime zdt = ZonedDateTime.ofInstant( instant , zoneId );
		return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(zdt.toString());
    }
}
