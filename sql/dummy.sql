-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 10-05-2021 a las 18:28:19
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dummy`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `post`
--

INSERT INTO `post` (`id`, `user_id`, `summary`, `date`) VALUES
(2, 2, 'Prueba de posts', '2021-03-30 22:00:00'),
(11, 9, 'Este es el detalle', '2021-05-07 17:11:27'),
(12, 9, 'Este es el detalle2', '2021-05-07 17:15:46'),
(13, 9, 'Este es el detalle3', '2021-05-07 11:46:15'),
(14, 9, 'Este es el detalle4', '2021-05-07 11:48:39'),
(15, 9, 'Este es el detalle5', '2021-05-07 11:50:18'),
(16, 9, 'Este es el detalle6', '2021-05-07 12:05:00'),
(17, 9, 'Este es el detalle7', '2021-05-07 12:34:53'),
(18, 9, 'Este es el detalle7', '2021-05-07 12:44:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email_code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `fullname`, `email`, `password`, `position`, `image`, `phone`, `email_code`) VALUES
(1, '', 'Angela Garcia', 'danig_1@hotmail.com', '', 'Software Engineer', 'https://cdn.pixabay.com/photo/2020/07/01/12/58/icon-5359554__340.png', '0', 0),
(2, '', 'Tatiana Gutierrez', 'qwe', '', 'Data Scientist', 'https://cdn.pixabay.com/photo/2020/07/01/12/58/icon-5359554__340.png', '0', 0),
(3, '', 'Nicolas Rojas Nino', 'nicolasrojasnino@imagineapp.com', '', 'CEO Imagine App', 'https://leportgolfcountry.com.ar/wp-content/uploads/2018/04/user-male-icon.png', '0', 0),
(9, 'dani', 'Daniela García', 'danigarcia_1@hotmail.com', '$2a$12$wzbtTHXg86vR0qAVa9i3wuE5s5eMEqCY.qFNv8phSWvCsZIdr3TgW', 'Team Leader', 'https://upload.wikimedia.org/wikipedia/commons/4/49/Koala_climbing_tree.jpg', '+573167938378', 1425),
(10, 'dani2', 'Daniela García', 'danigarcia_2@hotmail.com', '$2a$12$IMGmQrd0J//ttqBLLYxPS.vcIRobBT295sR2Kmo3a1FgIw01vVJEi', 'Team Leader', 'https://upload.wikimedia.org/wikipedia/commons/4/49/Koala_climbing_tree.jpg', '+573167938378', 0),
(11, 'dani3', 'Daniela García', 'danigarcia_3@hotmail.com', '$2a$12$IMGmQrd0J//ttqBLLYxPS.vcIRobBT295sR2Kmo3a1FgIw01vVJEi', 'Team Leader', 'https://upload.wikimedia.org/wikipedia/commons/4/49/Koala_climbing_tree.jpg', '+573167938378', 0),
(12, 'dani4', 'Daniela García', 'danigarcia_4@hotmail.com', '$2a$12$IMGmQrd0J//ttqBLLYxPS.vcIRobBT295sR2Kmo3a1FgIw01vVJEi', 'Team Leader', 'https://upload.wikimedia.org/wikipedia/commons/4/49/Koala_climbing_tree.jpg', '+573167938378', 0),
(13, 'tatis', 'Tatiana Gutiérrez', 'jtgutierrez@outlook.es', '$2a$12$CteN5Ng034IvrsC9LhLbIO2uEzhDDDsbHmba3tR.w.2Iy5idNBuD2', 'Backend Developer', 'https://upload.wikimedia.org/wikipedia/commons/4/49/Koala_climbing_tree.jpg', '+573167938378', 0),
(14, 'tatis2', 'Tatiana Gutiérrez', 'jtgutierrez2@gmail.com', '$2a$12$Tttwplv9UxGQZpO2gN6MDeuyvRKzlpRVecdsv/I51V5ji.2jNJ1ca', 'Backend Developer', 'https://upload.wikimedia.org/wikipedia/commons/4/49/Koala_climbing_tree.jpg', '+573167938378', NULL),
(15, 'tatis3', 'Tatiana Gutiérrez', 'jtgutierrez3@gmail.com', '$2a$12$ioVlu1OhbTIQA93p4CoQPug6A8yZRDO31Sbd9k7f//W8c2C5my3EK', 'Backend Developer', 'https://upload.wikimedia.org/wikipedia/commons/4/49/Koala_climbing_tree.jpg', '+573167938378', NULL),
(16, 'tatis4', 'Tatiana Gutiérrez', 'jtgutierrez4@gmail.com', '$2a$12$wro.FVbdcHlohRH2Zf1o/OdmuTuGFI2.Hrjaw7LXgSw2rixxFTgN.', 'Backend Developer', 'https://upload.wikimedia.org/wikipedia/commons/4/49/Koala_climbing_tree.jpg', '+573167938378', NULL),
(17, 'dani5', 'Daniela García', 'danigarcia_5@hotmail.com', '$2a$12$U6WR2eY/RAW/ezbOieHyJ.R6sa61bzTioapsOPvhfFn.tDzLpAuO2', 'Team Leader', 'https://upload.wikimedia.org/wikipedia/commons/4/49/Koala_climbing_tree.jpg', '+573167938378', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_post` (`user_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `fk_post` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
