/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angela.apidummy.repository;

import com.angela.apidummy.model.Post;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 *
 * @author ANGELA
 */
@Repository
public interface PostRepository extends JpaRepository<Post, Integer>{
    
	List<Post> findAllByOrderByIdDesc();
}
