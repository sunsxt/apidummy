package com.angela.apidummy.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.angela.apidummy.dto.LoginDto;
import com.angela.apidummy.dto.ReturnDto;
import com.angela.apidummy.dto.UserCreateDto;
import com.angela.apidummy.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;

	@ResponseBody
	@CrossOrigin(origins = "*", methods = { RequestMethod.POST })
	@PostMapping(path = "/create", consumes = "application/json")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<ReturnDto> create(@RequestBody UserCreateDto userCreateDto) {
		ReturnDto returnDto = new ReturnDto();
		try {
			userService.create(userCreateDto);
		} catch (Exception e) {
			e.printStackTrace();
			returnDto.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			returnDto.setResult("Error: " + e.getMessage());
			return new ResponseEntity<>(returnDto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		returnDto.setCode(String.valueOf(HttpStatus.OK.value()));
		returnDto.setResult("Se ha agregado correctamente");
		return new ResponseEntity<>(returnDto, HttpStatus.OK);
	}

	@ResponseBody
	@CrossOrigin(origins = "*", methods = { RequestMethod.POST })
	@PostMapping(path = "/login", consumes = "application/json")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<ReturnDto> login(@RequestBody LoginDto loginDto) {
		ReturnDto returnDto = new ReturnDto();
		try {
			ResponseEntity<ReturnDto> result = userService.login(loginDto);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			returnDto.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			returnDto.setResult("Error: " + e.getMessage());
			return new ResponseEntity<>(returnDto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@CrossOrigin(origins = "*", methods = { RequestMethod.POST })
	@PostMapping(path = "/send-code-email", consumes = "application/json")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<ReturnDto> sendCodeEmail(@RequestBody LoginDto loginDto) {
		ReturnDto returnDto = new ReturnDto();
		try {
			userService.sendEmailForRestorePassword(loginDto);
			returnDto.setCode(String.valueOf(HttpStatus.OK.value()));
			returnDto.setResult("Correct send email");
			return new ResponseEntity<>(returnDto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			returnDto.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			returnDto.setResult("Error: " + e.getMessage());
			return new ResponseEntity<>(returnDto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@CrossOrigin(origins = "*", methods = { RequestMethod.PUT })
	@PutMapping(path = "/updatePassword", consumes = "application/json")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<ReturnDto> changePassword(@RequestBody LoginDto loginDto) {
		ReturnDto returnDto = new ReturnDto();
		try {
			userService.updatePassword(loginDto);
			returnDto.setCode(String.valueOf(HttpStatus.OK.value()));
			returnDto.setResult("Correct update");
			return new ResponseEntity<>(returnDto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			returnDto.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			returnDto.setResult("Error: " + e.getMessage());
			return new ResponseEntity<>(returnDto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ResponseBody
	@CrossOrigin(origins = "*", methods = { RequestMethod.GET })
	@GetMapping("/email-code")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<ReturnDto> getEmailCode(@RequestParam(required = false) String username,
			@RequestParam(required = false) String email, @RequestParam(required = false) Integer code) {

		ReturnDto returnDto = new ReturnDto();
		try {
			String emailCode = String.valueOf(userService.validateEmailCode(username, email, code));
			returnDto.setCode(String.valueOf(HttpStatus.OK.value()));
			returnDto.setResult(emailCode);
			return new ResponseEntity<>(returnDto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			returnDto.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			returnDto.setResult("Error: " + e.getMessage());
			return new ResponseEntity<>(returnDto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
